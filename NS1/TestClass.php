<?php

namespace NS1;


use GoFinTech\Config\ConfigBuilder;

class TestClass
{
    public function test() {
        $config = ConfigBuilder::buildDefault();
        // The following parameter is inferred to be of type GoFinTech\Config\string
        // In 2018.2.5 it shows up in hint but otherwise does not cause problems
        // In 2018.3 it gives static analysis problem, so it seems that there was a bug
        // before 2018.3 but it became a problem in 2018.3
        $config->get("test");
    }
}